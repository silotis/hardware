# Copyright 2017-2019 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=hughsie ] \
    meson \
    python [ blacklist=2 multibuild=false has_lib=false ] \
    systemd-service \
    test-dbus-daemon \
    udev-rules \
    vala [ vala_dep=true with_opt=true ] \
    gtk-icon-cache

SUMMARY="A simple daemon to allow session software to update firmware"
HOMEPAGE+=" https://www.fwupd.org"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    dell [[
        description = [ Support for flashing firmware on DELL machines ]
        requires = [ efi ]
    ]]
    efi [[ description = [ Support for flashing firmware via UEFI ] ]]
    firmware-packager [[ description = [ Python script to create firmware packages ] ]]
    gobject-introspection
    gtk-doc
    systemd
    vapi [[ requires = [ gobject-introspection ] ]]
"

DEPENDENCIES="
    build:
        sys-apps/help2man
        sys-boot/efivar[>=33]
        sys-kernel/linux-headers[>=4.4-r1] [[ note = [ linux/nvme_ioctl.h for nvme plugin ] ]]
        virtual/pkg-config
        efi? (
            fonts/dejavu
            gnome-bindings/pygobject:3[python_abis:*(-)?]
            dev-python/Pillow[python_abis:*(-)?]
            dev-python/pycairo[python_abis:*(-)?]
            media-libs/fontconfig
            media-libs/freetype:2
            sys-boot/gnu-efi
            x11-libs/cairo
            x11-libs/pango[gobject-introspection]
        )
        gtk-doc? ( dev-doc/gtk-doc )
    build+run:
        app-arch/gcab [[ note = [ colorhug and rpiupdate plugin, firmware-packager script ] ]]
        app-arch/libarchive
        app-crypt/gpgme
        core/json-glib[>=1.1.1]
        dev-db/sqlite:3
        dev-libs/glib:2[>=2.45.8]
        dev-libs/libgpg-error
        dev-libs/libgusb[>=0.3.0]
        dev-libs/libxmlb[>=0.1.5]
        dev-util/elfutils
        gnome-desktop/libgudev
        gnome-desktop/libsoup:2.4[>=2.51.92][gobject-introspection?]
        sys-auth/polkit:1[>=0.113-r2] [[ note = [ itstools support ] ]]
        dell? ( sys-apps/libsmbios[>=2.4.0] )
        gobject-introspection? ( gnome-desktop/gobject-introspection:1 )
        systemd? ( sys-apps/systemd[>=231] )
    run:
        firmware-packager? (
            app-arch/p7zip
            dev-lang/python:*[>=3.5]
        )
"

MESON_SRC_CONFIGURE_PARAMS=(
    --localstatedir=/var
    -Ddaemon=true
    -Dconsolekit=false
    -Defi-cc=${CC}
    -Defi-includedir=/usr/$(exhost --target)/include/efi
    -Defi-ld=${LD}
    -Defi-libdir=/usr/$(exhost --target)/lib
    -Dgpg=true
    -Dlvfs=true
    -Dman=true
    -Dpkcs7=false
    -Dplugin_altos=true
    -Dplugin_amt=true
    -Dplugin_dummy=false
    -Dplugin_nvme=true
    -Dplugin_redfish=true
    -Dplugin_synaptics=true
    -Dplugin_thunderbolt=true
    -Dtests=false
    -Dsystemdunitdir=${SYSTEMDSYSTEMUNITDIR}
    -Dudevdir=${UDEVDIR}
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'efi plugin_uefi'
    'dell plugin_dell'
    firmware-packager
    'gobject-introspection introspection'
    'gtk-doc gtkdoc'
    systemd
)

src_test() {
    test-dbus-daemon_run-tests meson_src_test
}

src_install() {
    meson_src_install

    keepdir /var/lib/fwupd
}

