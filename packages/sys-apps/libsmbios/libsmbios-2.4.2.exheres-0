# Copyright 2010 Paul Seidler
# Distributed under the terms of the GNU General Public License v2

require github [ user=dell tag=v${PV} ] \
    flag-o-matic \
    autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

SUMMARY="Provide access to SMBIOS information"
DESCRIPTION="
The libsmbios project aims towards providing access to as much BIOS information as possible. It
does this by providing a library of functions that can be used as well as sample binaries.
"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    doc
    python [[ description = [ Build and install python utilities ] ]]
    ( linguas: de en en_US es fr it ja ko nl zh_CN zh_TW )
"

# needs pyunit
RESTRICT="test"

DEPENDENCIES="
    build:
        sys-apps/help2man
        sys-devel/gettext[>=0.14]
        virtual/pkg-config[>=0.9.0]
        doc? (
            app-doc/doxygen
            media-gfx/graphviz
        )
    build+run:
        dev-libs/libxml2:2.0
        python? ( dev-lang/python:*[>=3.0] )
    test:
        dev-cpp/cppunit
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-2.2.28-cppunit-tests.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-nls
    --disable-rpath
    --disable-static
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'python'
    'doc doxygen'
    'doc graphviz'
)

src_prepare() {
    # Do not build the yum plugin
    edo sed \
        -e '/yum-plugin/d' \
        -i Makefile.am

    edo autopoint --force

    autotools_src_prepare
}

src_configure() {
    replace-flags -O3 -O2

    default
}

src_install() {
    default

    insinto /usr/$(exhost --target)/include
    doins -r src/include/smbios_c
}

